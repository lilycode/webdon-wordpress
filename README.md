## webdon-wordpress

webdon-wordpress is a toolkit for rapid theme development for WordPress.

It's built on a couple of different components:

* [\_s (underscores)](https://github.com/automattic/_s) – the "starter theme" from the people behind Wordpress (Automattic).
* [Bourbon](https://github.com/thoughtbot/bourbon) – a framework from Thoughtbot.
* [Neat](https://github.com/thoughtbot/neat) – a grid for Bourbon, by Thoughtbot.

And then there are a couple of tools that basically does all the magic:

* [Grunt](http://gruntjs.com)
* [Compass](http://compass-style.org)
* [Bower](http://bower.io)

## Setup

You need a couple of tools to start.

* [Ruby](https://www.ruby-lang.org) (with [Bundler](http://bundler.io))
* [node.js](http://nodejs.org) (with [npm](https://www.npmjs.org))

```bash
$ rbenv install # install a ruby
$ bundle install # install gems
$ npm install -g bower # install bower
$ npm install -g grunt-cli # install grunt
$ npm install # install local requirements
$ bower install # install foundation and stuff
```